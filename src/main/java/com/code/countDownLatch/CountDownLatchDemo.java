package com.code.countDownLatch;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchDemo {
    //juc包下的CountDownLatch的Demo

    public static void main(String[] args) {

        CountDownLatch c = new CountDownLatch(6);

        for(int i = 1; i <= 6; i++){
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"国被灭啦");
                c.countDown();//计数器依次减小
            },Country.getCNameByI(i)).start();
        }

        try {
            c.await();
            System.out.println("秦国统一六国");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
