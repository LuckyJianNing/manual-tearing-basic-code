package com.code.countDownLatch;

public enum Country {
    ONE(1,"齐国"),TWO(2,"楚国"),THREE(3,"燕国"),FORE(4,"赵国"),FIVE(5,"韩国"),SIX(6,"魏国");

    public int i;
    public String CName;

    public int getI(){
        return i;
    }

    public String getCName(){
        return CName;
    }

    Country(int i, String CName) {
        this.i = i;
        this.CName = CName;
    }

    //通过编号获取国家名称
    public static String getCNameByI(int i){
        Country[] values = Country.values();
        for (Country value : values) {
            if(i == value.getI()){
                return value.CName;
            }
        }
        return null;
    }
}
