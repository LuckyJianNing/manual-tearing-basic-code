package com.code.deadLock;

import java.util.concurrent.TimeUnit;

public class DeadLockDemo {

    public static void main(String[] args) {
        String lockA = "lockA";
        String lockB = "lockB";

//        new Thread(new HoldLockThread(lockA,lockB)).start();
//        new Thread(new HoldLockThread(lockB,lockA)).start();

        new Thread(()->{
            synchronized (lockA){
                System.out.println(Thread.currentThread().getName()+"自己持有"+lockA+"尝试获取"+lockB);
                //睡两秒更清除
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lockB){

                }
            }
        },"A").start();


        new Thread(()->{
            synchronized (lockB){
                System.out.println(Thread.currentThread().getName()+"自己持有"+lockB+"尝试获取"+lockA);
                //睡两秒更清除
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lockA){

                }
            }
        },"B").start();
    }
}
