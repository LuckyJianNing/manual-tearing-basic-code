package com.code.abstractFactoryPattern.color;

public interface Color {
    public void show();
}
