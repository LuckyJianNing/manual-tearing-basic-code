package com.code.abstractFactoryPattern.color;

import com.code.abstractFactoryPattern.AbstractFactory;
import com.code.abstractFactoryPattern.shape.Shape;

public class ColorFactory extends AbstractFactory {
    @Override
    public Shape getShape(String shape) {
        return null;
    }

    @Override
    public Color getColor(String color) {

        if( color == null){
            return null;
        }else if( color.equalsIgnoreCase("RED")){
            return new Red();
        }else if( color.equalsIgnoreCase("BULE")){
            return new Blue();
        }else{
            return new Green();
        }
    }
}
