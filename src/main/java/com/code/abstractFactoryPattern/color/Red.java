package com.code.abstractFactoryPattern.color;

public class Red implements Color{
    @Override
    public void show() {
        System.out.println("show red");
    }
}
