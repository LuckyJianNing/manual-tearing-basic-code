package com.code.abstractFactoryPattern.color;

public class Blue implements Color{
    @Override
    public void show() {
        System.out.println("show blue");
    }
}
