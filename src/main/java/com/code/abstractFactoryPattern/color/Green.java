package com.code.abstractFactoryPattern.color;

public class Green implements Color{
    @Override
    public void show() {
        System.out.println("show green");
    }
}
