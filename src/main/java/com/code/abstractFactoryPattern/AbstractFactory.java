package com.code.abstractFactoryPattern;

import com.code.abstractFactoryPattern.color.Color;
import com.code.abstractFactoryPattern.shape.Rectangle;
import com.code.abstractFactoryPattern.shape.Shape;



public  abstract class AbstractFactory {

    public abstract Shape getShape(String shape);

    public abstract Color getColor(String color);
}
