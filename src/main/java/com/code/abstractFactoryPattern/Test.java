package com.code.abstractFactoryPattern;

import com.code.abstractFactoryPattern.color.Color;
import com.code.abstractFactoryPattern.color.ColorFactory;
import com.code.abstractFactoryPattern.shape.Circle;
import com.code.abstractFactoryPattern.shape.Shape;
import com.code.abstractFactoryPattern.shape.ShapeFactory;

public class Test {
    public static void main(String[] args) {
        AbstractFactory abstractFactory1 = new ColorFactory();
        Color c =  abstractFactory1.getColor("RED");
        c.show();

        AbstractFactory abstractFactory2 = new ShapeFactory();
        Shape circle = abstractFactory2.getShape("CIRCLE");
        circle.draw();
    }
}
