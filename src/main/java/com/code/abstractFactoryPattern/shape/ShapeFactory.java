package com.code.abstractFactoryPattern.shape;

import com.code.abstractFactoryPattern.AbstractFactory;
import com.code.abstractFactoryPattern.color.Color;

public class ShapeFactory extends AbstractFactory {

    @Override
    public Shape getShape(String shape) {

        if(shape == null){
            return null;
        }else if(shape.equalsIgnoreCase("CIRCLE")){
            return new Circle();
        }else if(shape.equalsIgnoreCase("RECTANGLE")){
            return new Rectangle();
        }else{
            return new Square();
        }

    }

    @Override
    public Color getColor(String color) {
        return null;
    }
}
