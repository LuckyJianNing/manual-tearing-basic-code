package com.code.abstractFactoryPattern.shape;

public interface Shape {
    public void draw();
}
