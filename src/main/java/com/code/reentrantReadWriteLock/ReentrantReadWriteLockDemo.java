package com.code.reentrantReadWriteLock;

import java.util.Arrays;

public class ReentrantReadWriteLockDemo {
    public static void main(String[] args) {
        MyCache myCache = new MyCache();

        //线程操作资源类，5个线程写
        for(int i = 1; i <= 5; i++){
            //lambda表达式中必须是final变量
            final int tempInt = i;
            new Thread(()->{
                myCache.put(tempInt, String.valueOf(tempInt));
            },String.valueOf(i)).start();
        }

        //线程操作资源类，5个线程读
        for(int i = 1; i <= 5; i++){
            //lambda表达式中必须是final变量
            final int tempInt = i;
            new Thread(()->{
                myCache.get(tempInt);
            },String.valueOf(i)).start();
        }
    }
}
