package com.code.adapterPattern;

public class VlcPlayer implements AdvanceMediaPlayer{
    @Override
    public void playVlc(String fileName) {
        System.out.println("播放Vlc" + fileName);
    }

    @Override
    public void playMp4(String fileName) {

    }
}
