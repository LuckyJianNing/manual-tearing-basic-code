package com.code.adapterPattern;

public class AudioPlayer implements MediaPlayer{

    @Override
    public void play(String audioType,String fileName) {
        MediaAdapter mediaAdapter;
        if(audioType.equalsIgnoreCase("mp3")){
            System.out.println("播放MP3" + fileName);
        }else{
            mediaAdapter = new MediaAdapter();
            mediaAdapter.play(audioType,fileName);
        }
    }
}
