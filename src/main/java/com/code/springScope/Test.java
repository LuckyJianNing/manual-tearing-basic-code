package com.code.springScope;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-config.xml");

        Student student1 = applicationContext.getBean("student", Student.class);

        Student student2 = applicationContext.getBean("student", Student.class);

        //没有重写equals方法，比较地址
        System.out.println(student2 == student1);
    }
}
