package com.code.copyOnWrite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class copyOnWriteArrayListDemo {

    public static void main(String[] args) {

//        ArrayList<String> list = new ArrayList<>();
//        List<String> strings = Collections.synchronizedList(list);
        List<String> list = new CopyOnWriteArrayList();

        for(int i = 0; i <= 9; i++){
            new Thread(()->{
                //向集合添加内容
                list.add(UUID.randomUUID().toString().substring(0,8));

                //输出集合内容
                System.out.println(list);
            }).start();
        }
        System.out.println(Runtime.getRuntime().availableProcessors());
    }
}
