package com.code.sort;

import java.util.Arrays;

public class BubbleSort {
    public static int[] sort(int[] arr){

        for(int i = 1; i < arr.length; i++){
            for(int j = 0; j < arr.length-i; j++){
                if(arr[j] > arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }

        return arr;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{4,3,8,5,6};
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
