package com.code.sort;

import java.util.Arrays;

public class SelectionSort {
    public static int[] sort(int[] arr){
        int min;
        int k = 0;
        for(int i = 0; i < arr.length-1; i++){
            min = arr[i];
            for(int j = i+1; j < arr.length; j++){
                if(arr[j] < min){
                    min = arr[j];
                    k = j;
                }
            }
            arr[k] = arr[i];
            arr[i] = min;

        }
        return arr;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{4,3,8,5,6};
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
