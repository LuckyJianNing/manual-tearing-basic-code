package com.code.cyclicBarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierDemo {
    public static void main(String[] args) {
        //六个人开会，只有六个人都到齐了才能开会
        CyclicBarrier cyclicBarrier = new CyclicBarrier(6,()->{
            System.out.println("人数到齐，我们开会");
        });

        for(int i = 1; i <= 6; i++){
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"号工作人员到了会议室");
                try {
                    cyclicBarrier.await();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            },String.valueOf(i)).start();
        }


    }
}
