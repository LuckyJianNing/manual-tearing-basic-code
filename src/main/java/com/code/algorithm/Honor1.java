package com.code.algorithm;

import java.util.Arrays;

public class Honor1 {

    /**
     * 输入一个字符串和一个整型
     * 整型为字符串内部需要排序的子字符串个数
     * 0开头倒序
     * 1开头正序
     * 例子：
     *      输入： 2
     *            "0abcdefg1abcdefg"
     *      输出："gfedcba abcdefg"
     *
     * @param args
     */

    public static void main(String[] args) {

//        Scanner scanner1 = new Scanner(System.in);
//        int a = scanner1.nextInt();
//        Scanner scanner2 = new Scanner(System.in);
//        String s = scanner2.nextLine();
        int a = 1;
        String s = "0zxcvb";

        System.out.println(method(a,s));
    }

    /**
     * 将字符串分开，搞个字符串数组出来
     * @param n 要排序的子字符串个数
     * @param s 给定的字符串
     * @return
     */
    public static String method(int n, String s) {
        //n为1的情况
        if(n == 1){
            String[] s1 = new String[1];
            s1[0] = s;
            return sort(1,s1);
        }
        //找出字符串种以0，1开头的子串，先找0，1下标
        char[] chars = s.toCharArray();
        int[] arr = new int[n];
        int t = 0;
        for (int i = 0; i < s.length(); i++) {
            if (chars[i] == '0' || chars[i] == '1') {
                arr[t++] = i;
            }
        }
        System.out.println(Arrays.toString(arr));
        //根据0，1下标，创建字符串数组
        StringBuilder sB = new StringBuilder(s);
        String[] temp = new String[n];
        for (int i = 0; i < n - 1; i++) {
            temp[i] = sB.substring(arr[i], arr[i + 1]);
        }
        temp[n - 1] = sB.substring(arr[n - 1]);
        System.out.println(temp[0] + temp[1] + temp[2]);

        return sort(n, temp);

    }

    /**
     *  对字符串数组里的子字符串进行排序
     * @param n
     * @param temp
     * @return
     */
    public static String sort(int n, String[] temp) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < n; i++) {
            if (temp[i].charAt(0) == '1') {
                String[] split = temp[i].split("1");
                char[] chars1 = split[1].toCharArray();
                Arrays.sort(chars1);
                String a = new String(chars1);
                res.append(a);
                res.append(" ");
            } else {
                String[] split = temp[i].split("0");
                char[] chars1 = split[1].toCharArray();
                Arrays.sort(chars1);
                char[] chars2 = new char[chars1.length];
                for (int j = chars1.length - 1, k = 0; j >= 0; j--, k++) {
                    chars2[k] = chars1[j];
                }
                String a = new String(chars2);
                res.append(a);
                res.append(" ");
            }
        }
        return res.substring(0,res.length()-1);
    }
}

