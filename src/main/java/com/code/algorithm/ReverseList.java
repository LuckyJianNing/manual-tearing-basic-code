package com.code.algorithm;

import org.junit.Test;

public class ReverseList {
    public static class ListNode {
        int val;
        ListNode next = null;

        ListNode(int val) {
            this.val = val;
        }
    }
    public static ListNode reverseList(ListNode head){
        System.out.println(head);
        ListNode p = head;
        ListNode res = new ListNode(0);
        while(p != null){
            ListNode n = new ListNode(p.val);//涉及到了引用传递，所以需要新造一个Node
            n.next = res.next;
            res.next = n;
            p = p.next;

        }
        return res.next;
    }
}

class Main{
    public static void main(String[] args) {
        ReverseList.ListNode n1 = new ReverseList.ListNode(1);
        ReverseList.ListNode n2 = new ReverseList.ListNode(2);
        ReverseList.ListNode n3 = new ReverseList.ListNode(3);

        n1.next = n2;
        n2.next = n3;
        System.out.println(n1);
        ReverseList.reverseList(n1);
    }



}

