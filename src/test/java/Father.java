public class Father {
    private String a = "father";
    public Father(){
        say();
    }
    public void say(){
        System.out.println("i am father : " + a);
    }
}

class Sub extends Father{
    private String b = "son";
    public void say(){
        System.out.println("i am son : "+b);
    }
}

class test{
    public static void main(String[] args) {
        Father f = new Father();
        Sub s = new Sub();
    }
}
