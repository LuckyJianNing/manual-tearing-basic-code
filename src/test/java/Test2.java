public class Test2 {
    public static void main(String[] args) {
        String s1 = "hello2";
        String s2 = "hello";
        String s3 = s2+"2";//在堆中
        System.out.println(s1==s3);
        String s4 = "hello2";//在字符串常量池中
        System.out.println(s1==s4);
    }
}
