import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

public class Test1 {
    /**
     * java值传递
     */

    public void add(int a, int b){
        int temp = a;
        a = b;
        b = temp;
    }

    @Test
    public void test1(){
        int num1 = 1;
        int num2 = 2;
        System.out.println("num1:"+num1+"----num2:"+num2);
        add(num1,num2);
        System.out.println("num1:"+num1+"----num2:"+num2);
    }


    public void add(Integer a, Integer b){
        a = a+1;
        b = b+1;
    }

    @Test
    public void test2(){
        Integer num1 = new Integer(1);
        Integer num2 = new Integer(2);

        System.out.println("num1:"+num1+"----num2:"+num2);
        add(num1,num2);
        System.out.println("num1:"+num1+"----num2:"+num2);

    }

    class Student{
        int id;
        StringBuilder name;

        public Student(int id, StringBuilder name){
            this.id = id;
            this.name = name;
        }

    }

    public void idAdd(Student s){
        System.out.println(s);
        s.id = s.id++;
        StringBuilder q = new StringBuilder("qwe");
        s.name = q;
    }
    @Test
    public void test3(){
        StringBuilder q = new StringBuilder("aaa");
        Student a = new Student(1,q);
        System.out.println(a);
        idAdd(a);
        System.out.println(a);
    }

    @Test
    public void test4(){
        int n = 14;
        int m = 21;
        System.out.println(n&m);
    }

    @Test
    public void test5(){
        ArrayList<Character> a = new ArrayList<>();

        a.add('a');
        a.add('b');
        a.add('c');
        //indexOf()返回该值所对应的数组下标
        System.out.println(a.indexOf('b'));
        //set()替换某位置上的某元素
        System.out.println(a.get(0));
        a.set(0,'d');
        System.out.println(a.get(0));

        Integer b = new Integer(1);
        int c = b;
        String s = "123";
        System.out.println(Integer.parseInt(s));

        //java中通过补码来表示负数
        byte i = -7;
        System.out.println(Integer.toBinaryString(i));
        char d = '冯';
        char e = 'a';


        Scanner s1 = new Scanner(System.in);
        int a1=0,b1=0;
        while(s1.hasNextInt()){
            a1 = s1.nextInt();
            b1 = s1.nextInt();
        }
        System.out.println(a1+b1);
    }

    @Test
    public void test6(){
        String s = "123";
        StringBuilder sb = new StringBuilder();
        Stack<Character> stack = new Stack<>();
        for(int i = 0 ; i < s.length(); i++){
            if(i == 0 && s.charAt(0) == '-'){
                continue;
            }
            stack.push(s.charAt(i));
        }
        if(s.charAt(0) == '-'){
            sb.append('-');
        }
        for(int i = 0; i < s.length(); i++){
            if(i == 0 && s.charAt(0) == '-'){
                continue;
            }
            sb.append(stack.pop());
        }
        System.out.println(sb);

    }

    public int maxSum(int[] arr){
        int max = Integer.MIN_VALUE;
        int temp = 0;
        for(int i = 0; i < arr.length; i++){
            temp = temp+arr[i];
            if(temp < 0){
                temp = 0;
            }else{
                if(temp > max){
                    max = temp;
                }
            }
        }
        return max;
    }

    @Test
    public void test7(){
        int[] arr = new int[]{-3,-6,-2,-2,-7,-10,-2,-2,-2};
        System.out.println(maxSum(arr));
    }
}
